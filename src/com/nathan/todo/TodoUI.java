package com.nathan.todo;

import javax.servlet.annotation.WebServlet;

import com.nathan.todo.model.AppUser;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("todo")
public class TodoUI extends UI {

    private CssLayout root = new CssLayout();

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = TodoUI.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        setContent(root);
        root.addStyleName("root");
        root.setSizeFull();

        setLoggedInUser(null);
    }

    public void setLoggedInUser(AppUser user) {
        root.removeAllComponents();
        if (user == null) {
            root.addComponent(new LoginView(this));
        } else {
            root.addComponent(new TodoView(this, user));
        }
    }
}