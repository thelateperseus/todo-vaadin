package com.nathan.todo;

import com.nathan.todo.model.AppUser;
import com.nathan.todo.model.TodoList;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class NewListDialog extends Window {
    TextField nameField = new TextField("Name");

    public NewListDialog(AppUser owner, JPAContainer<TodoList> lists, Tree menuTree) {
        super("Create New List");

        center();
        setDraggable(false);
        setResizable(false);
        setModal(true);
        setWidth("600px");

        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        setContent(content);

        FormLayout form = new FormLayout();
        form.setWidth("100%");
        content.addComponent(form);

        nameField.setWidth("100%");
        form.addComponent(nameField);
        nameField.focus();

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setWidth("100%");
        content.addComponent(buttons);

        Button saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            TodoList entity = new TodoList(nameField.getValue());
            entity.setOwner(owner);
            Object entityId = lists.addEntity(entity);
            menuTree.setValue(entityId);
            close();
        });
        buttons.addComponent(saveButton);
        buttons.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
        buttons.setExpandRatio(saveButton, 1.0f);

        Button cancelButton = new Button("Cancel");
        cancelButton.addClickListener(event -> close());
        buttons.addComponent(cancelButton);
        buttons.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
    }
}