package com.nathan.todo;

import javax.persistence.EntityManager;
import javax.servlet.annotation.WebServlet;

import org.vaadin.dialogs.ConfirmDialog;

import com.nathan.todo.model.AppUser;
import com.nathan.todo.model.TodoItem;
import com.nathan.todo.model.TodoList;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Runo;

@SuppressWarnings("serial")
@Theme("todo")
public class TodoView extends VerticalLayout {

    private JPAContainer<TodoList> lists;
    private JPAContainer<TodoItem> items;
    private AppUser user;
    private TodoUI ui;

    public TodoView(TodoUI ui, AppUser user) {
        this.user = user;
        this.ui = ui;
        initUI();
    }

    private void initUI() {
        setSizeFull();

        // Title bar
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(true);
        header.addStyleName("titlebar");
        header.setWidth("100%");
        VerticalLayout titlebox = new VerticalLayout();
        titlebox.setSizeUndefined();
        //titlebox.addStyleName("titlebox");
        Label title = new Label("Todo Vaadin");
        title.addStyleName(Runo.LABEL_H1);
        title.setWidth(null);
        titlebox.addComponent(title);
        header.addComponent(titlebox);
        header.setComponentAlignment(titlebox, Alignment.TOP_LEFT);
        
        // Right part of title bar
        VerticalLayout userbox = new VerticalLayout();
        Button logout = new Button("Sign Out");
        userbox.addComponent(logout);
        userbox.setComponentAlignment(logout, Alignment.MIDDLE_RIGHT);
        userbox.setHeight("100%"); // Should take height from the titlebox
        header.addComponent(userbox);

        addComponent(header);


        final HorizontalLayout content = new HorizontalLayout();
        content.setMargin(true);
        content.setSizeFull();

        EntityManager em = 
            JPAContainerFactory.createEntityManagerForPersistenceUnit("todolists");
        lists = JPAContainerFactory.make(TodoList.class, em);
        lists.addContainerFilter(new Compare.Equal("owner", user));
        lists.sort(new String[]{"name"}, new boolean[]{true});

        VerticalLayout listLayout = new VerticalLayout();
        Tree menuTree = new Tree("Lists", lists);
        menuTree.setItemCaptionPropertyId("name");
        listLayout.setWidth("200px");
        listLayout.addComponent(menuTree);

        listLayout.addComponent(new Label("&nbsp;",ContentMode.HTML));
        Button newListButton = new Button("New List");
        listLayout.addComponent(newListButton);

        newListButton.addClickListener(event -> {
            NewListDialog sub = new NewListDialog(user, lists, menuTree);
            UI.getCurrent().addWindow(sub);
        });
        
        content.addComponent(listLayout);

        items = JPAContainerFactory.make(TodoItem.class, em);
        items.sort(new String[]{"text", "id"}, new boolean[]{true, true});

        // Layout for the right hand panel
        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSizeFull();
        content.addComponent(tableLayout);
        content.setExpandRatio(tableLayout, 1.0f);

        HorizontalLayout crudButtons = new HorizontalLayout();
        crudButtons.setMargin(true);
        tableLayout.addComponent(crudButtons);

        Button newItemButton = new Button("New");
        crudButtons.addComponent(newItemButton);

        Button editItemButton = new Button("Edit");
        crudButtons.addComponent(editItemButton);

        Button deleteItemButton = new Button("Delete");
        crudButtons.addComponent(deleteItemButton);

        Table itemTable = new Table(null, items);
        itemTable.setColumnHeader("done", "Done");
        itemTable.setColumnHeader("text", "Description");
        itemTable.setColumnHeader("reminder", "Reminder");
        itemTable.setVisibleColumns(new Object[]{"done", "text", "reminder"});
        itemTable.setSizeFull();
        itemTable.setSelectable(true);
        tableLayout.addComponent(itemTable);
        tableLayout.setExpandRatio(itemTable, 1.0f);

        addComponent(content);
        setExpandRatio(content, 1.0f);

        // Add behaviours
        menuTree.setImmediate(true);
        menuTree.addValueChangeListener(event -> {
            Long listId = (Long) event.getProperty().getValue();
            TodoList list = lists.getItem(listId).getEntity();
            items.removeAllContainerFilters();
            items.addContainerFilter(new Compare.Equal("list", list));
        });

        newItemButton.addClickListener(event -> { 
            TodoItem item = new TodoItem();
            TodoList list = lists.getItem(menuTree.getValue()).getEntity();
            item.setList(list);
            final EntityItem<TodoItem> newItem = items.createEntityItem(item);
            TodoItemEditor itemEditor = new TodoItemEditor(items, newItem);
            UI.getCurrent().addWindow(itemEditor);
        });

        editItemButton.addClickListener(event -> { 
            Object selectedItemId = itemTable.getValue();
            if (selectedItemId == null) {
                Notification.show("Please select an item", Notification.Type.HUMANIZED_MESSAGE);
                return;
            }
            final EntityItem<TodoItem> item = items.getItem(selectedItemId); 
            TodoItemEditor itemEditor = new TodoItemEditor(items, item);
            UI.getCurrent().addWindow(itemEditor);
        });

        deleteItemButton.addClickListener(event -> { 
            Object selectedItemId = itemTable.getValue();
            if (selectedItemId == null) {
                Notification.show("Please select an item", Notification.Type.HUMANIZED_MESSAGE);
                return;
            }
            ConfirmDialog.show(UI.getCurrent(), "Delete this item?", dialog -> {
                if (dialog.isConfirmed()) {
                    items.removeItem(selectedItemId);
                }
            });
        });

        itemTable.addItemClickListener(event -> {
            if (event.isDoubleClick()) {
                final EntityItem<TodoItem> item = (EntityItem<TodoItem>) event.getItem(); 
                TodoItemEditor itemEditor = new TodoItemEditor(items, item);
                UI.getCurrent().addWindow(itemEditor);
            }
        });
        
        logout.addClickListener(event -> {
            ui.setLoggedInUser(null);
        });

        // Select first list
        Object firstId = lists.getIdByIndex(0);
        Long initialListId = lists.getItem(firstId).getEntity().getId();
        menuTree.setValue(initialListId);
    }
}