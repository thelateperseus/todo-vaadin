package com.nathan.todo;

import com.nathan.todo.model.TodoItem;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class TodoItemEditor extends Window { 

    private FieldGroup editorForm; 
    private Button saveButton; 
    private Button cancelButton; 
    
    public TodoItemEditor(JPAContainer<TodoItem> items, EntityItem<TodoItem> todoItem) { 
        super(todoItem.getEntity().getId() == null ? "Create New Item" : "Edit Item");

        center();
        setDraggable(false);
        setResizable(false);
        setModal(true);
        setWidth("600px");

        VerticalLayout content = new VerticalLayout();
        content.setMargin(true);
        setContent(content);

        FormLayout form = new FormLayout();
        form.setWidth("100%");
        content.addComponent(form);

        editorForm = new ValidatorFieldGroup<>(TodoItem.class, todoItem);
        editorForm.setBuffered(true);

        editorForm.setItemDataSource(todoItem);
        
        TextField textField = (TextField) editorForm.buildAndBind("text");
        textField.setWidth("100%");
        // Ensure "null" doesn't appear in the text field - Vaadin should have taken care of this for us :(
        if (textField.getValue() == null) {
            textField.setValue("");
        }
        textField.focus();
        form.addComponent(textField);

        DateField reminderField = (DateField) editorForm.buildAndBind("reminder");
        reminderField.setWidth("100%");
        form.addComponent(reminderField);

        Field<?> doneField = editorForm.buildAndBind("done");
        form.addComponent(doneField);

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setWidth("100%");
        content.addComponent(buttons);

        saveButton = new Button("Save");
        buttons.addComponent(saveButton);
        saveButton.addClickListener(event -> {
            try {
                editorForm.commit();
                if (todoItem.getEntity().getId() == null) {
                    items.addEntity(todoItem.getEntity());
                    Notification.show("New item created.", Notification.Type.TRAY_NOTIFICATION);
                } else {
                    Notification.show("Item saved.", Notification.Type.TRAY_NOTIFICATION);
                }
            } catch (Exception e) {
                if (e.getCause() instanceof InvalidValueException) {
                    return;
                }
                e.printStackTrace();
                Notification.show("Couldn't commit values: " + e.getMessage(),
                                  Notification.Type.ERROR_MESSAGE);
            }
            close();
        });
        buttons.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
        buttons.setExpandRatio(saveButton, 1.0f);

        cancelButton = new Button("Cancel");
        buttons.addComponent(cancelButton);
        cancelButton.addClickListener(event -> {
            editorForm.discard();
            close();
        });
        buttons.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
    }
}
