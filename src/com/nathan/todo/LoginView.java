package com.nathan.todo;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.mindrot.jbcrypt.BCrypt;

import com.nathan.todo.model.AppUser;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.event.ShortcutListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class LoginView extends VerticalLayout {

    private TodoUI ui;

    public LoginView(TodoUI ui) {
        this.ui = ui;
        initUI();
    }

    private void initUI() {
        setSizeFull();

        final CssLayout loginPanel = new CssLayout();
        loginPanel.addStyleName("login-panel");

        HorizontalLayout labels = new HorizontalLayout();
        labels.setMargin(true);
        labels.addStyleName("labels");
        loginPanel.addComponent(labels);

        Label title = new Label("Todo Vaadin App");
        title.setSizeUndefined();
        title.addStyleName("h2");
        title.addStyleName("light");
        labels.addComponent(title);
        labels.setComponentAlignment(title, Alignment.MIDDLE_LEFT);

        HorizontalLayout fields = new HorizontalLayout();
        fields.setSpacing(true);
        fields.setMargin(true);
        fields.addStyleName("fields");

        final TextField username = new TextField("Email");
        username.focus();
        fields.addComponent(username);

        final PasswordField password = new PasswordField("Password");
        fields.addComponent(password);

        final Button signin = new Button("Sign In");
        fields.addComponent(signin);
        fields.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);

        final ShortcutListener enter = new ShortcutListener("Sign In",
                KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                signin.click();
            }
        };

        signin.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                EntityManager em = 
                    JPAContainerFactory.createEntityManagerForPersistenceUnit("todolists");
                if (loginPanel.getComponentCount() > 2) {
                    // Remove the previous error message
                    loginPanel.removeComponent(loginPanel.getComponent(2));
                }
                try {
                    AppUser user = 
                        (AppUser)em.createQuery("select u from AppUser as u where u.email = :email")
                            .setParameter("email", username.getValue())
                            .getSingleResult();
                    if (BCrypt.checkpw(password.getValue(), user.getPasswordHash())) {
                        signin.removeShortcutListener(enter);
                        ui.setLoggedInUser(user);
                    }
                } catch (NoResultException e) { }

                // Add new error message
                Label error = new Label("Wrong username or password.");
                error.addStyleName("error");
                error.setSizeUndefined();
                error.addStyleName("light");
                // Add animation
                error.addStyleName("v-animate-reveal");
                loginPanel.addComponent(error);
                username.focus();
            }
        });

        signin.addShortcutListener(enter);

        loginPanel.addComponent(fields);

        addComponent(loginPanel);
        setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);
    }
}
