package com.nathan.todo;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.Field;

public class ValidatorFieldGroup<T> extends FieldGroup {
    private static final long serialVersionUID = 1L;

    private Class<T> clazz;

    public ValidatorFieldGroup(Class<T> clazz, EntityItem<T> item) {
        super(item);
        this.clazz = clazz;
    }

    @Override
    protected void configureField(Field<?> field) {
        super.configureField(field);
        BeanValidator validator = new BeanValidator(clazz, getPropertyId(field).toString());
        field.addValidator(validator);
        if (field.getLocale() != null) {
            validator.setLocale(field.getLocale());
        }
    }
}
